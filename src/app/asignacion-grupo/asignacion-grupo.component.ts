import { Component, OnInit } from '@angular/core';
import { AsignacionGrupo } from './asignacion-grupo';
import { AsignacionGrupoService } from './asignacion-grupo.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Profesor } from '../login/profesor';
import { ProfesorService} from '../login/profesor.service';

@Component({
  selector: 'app-asignacion-grupo',
  templateUrl: './asignacion-grupo.component.html',
  styleUrls: ['./asignacion-grupo.component.css']
})
export class AsignacionGrupoComponent implements OnInit {
  asignacionGrupoProfesor:  AsignacionGrupo[];
 // asginacionGrupos: AsignacionGrupo[];
 
 // id=13;
public identity;

  constructor(private asignaciongrupoService: AsignacionGrupoService, private router: Router, private activatedRoute: ActivatedRoute ) { 

    this.identity = JSON.parse(localStorage.getItem('identity'));
    console.log(this.identity.id);
  }


  ngOnInit() {



    //this.asignaciongrupoService.getAsignacionGrupos().subscribe(
    //asginacionGrupos => this.asginacionGrupos = asginacionGrupos
    //);

    //this.cargarAsignacionProfesor()

  this.asignaciongrupoService.getAsignacionGruposProfesor(this.identity.id).subscribe(
    asignacionGrupoProfesor => this.asignacionGrupoProfesor = asignacionGrupoProfesor
  );
  
  }
  

  //cargarAsignacionProfesor(id: number): void {
   // this.asignaciongrupoService.getAsignacionGrupoProfesor(this.id).subscribe((asignacionGrupoProfesor) => this.asignacionGrupoProfesor = asignacionGrupoProfesor);
  //}




}
