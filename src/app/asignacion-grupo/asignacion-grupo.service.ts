import { Injectable } from '@angular/core';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AsignacionGrupo } from './asignacion-grupo';

@Injectable({
  providedIn: 'root'
})
export class AsignacionGrupoService {
  private urlEndPoint: string = 'http://localhost:8080/api/asignacionGrupo';
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  
  
  constructor(private http: HttpClient) { }

  getAsignacionGrupos(): Observable<AsignacionGrupo[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map((response) => response as AsignacionGrupo[])
    );
  }

  // getAsignacionGrupoProfesor(id: number): Observable<AsignacionGrupo>{
  // return this.http.get<AsignacionGrupo>(`${this.urlEndPoint}Profesor/${id}`)
  //}

  getAsignacionGruposProfesor(id: number): Observable<AsignacionGrupo[]> {
    return this.http.get(`${this.urlEndPoint}Profesor/${id}`).pipe(
      map((response) => response as AsignacionGrupo[])
    );
  }

}
