import { Router } from "@angular/router";

import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { map, filter, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router: Router) { } 

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    
    if (req.headers.get('No-Auth') == "True")
            return next.handle(req.clone());

        if (localStorage.getItem('userToken') != null) {
            const clonedreq = req.clone({
                headers: req.headers.set("Authorization",localStorage.getItem('userToken'))
            });


            return next.handle(clonedreq).pipe(
                tap(event => {
                    console.log('EVENT=>',event);
                }, error => {
                  console.error('NICE ERROR', error);
                })
            );

            

        } else {
            this.router.navigateByUrl('/login');
        }

  }

}
