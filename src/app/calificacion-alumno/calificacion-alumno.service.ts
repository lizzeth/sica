import { Injectable } from '@angular/core';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { CalificacionAlumno } from './calificacion-alumno';

@Injectable({
  providedIn: 'root'
})
export class CalificacionAlumnoService {

  private urlEndPoint: string = 'http://localhost:8080/api/alumnos';
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  getAlumnosByCalificacion(id: number): Observable<CalificacionAlumno[]> {
    return this.http.get(`${this.urlEndPoint}/${id}`).pipe(
      map((response) => response as CalificacionAlumno[])
    );
  }
}
