import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent  {


    public profesor: any = { nombre:'Bernardo Huicochea' }
    @Input() public isUserLoggedIn: boolean;
    public identity;

    constructor( private router: Router) { 

      this.identity = JSON.parse(localStorage.getItem('identity'));
      console.log(this.identity.nombre);
    }
     
    Logout(){
      localStorage.removeItem('userToken');
      this.router.navigate(['/login']);
    }
  

}
