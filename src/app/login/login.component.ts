import { Component, OnInit, NgModule } from '@angular/core';
import { Profesor} from './profesor'; 
import { ProfesorService } from './profesor.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public profesor: Profesor;
  public errorMessage;

  //constructor(private usuarioService: UsuarioService, private router: Router,private toasty: NotifyService) {
  constructor(private usuarioService: ProfesorService, private router: Router) {
  	//this.usuario = new Usuario('','','','','','',this.rol);
  	this.profesor = new Profesor('','','','','','');
  }

  ngOnInit() {
  }

   onSubmit(){
  
  	this.usuarioService.login(this.profesor).subscribe(
      (data: any)  =>  {
       // console.log(data);
        localStorage.setItem('userToken',data.token);
        localStorage.setItem('identity', JSON.stringify(data.usuario));
        console.log(data);
        this.router.navigate(['asignacionGrupoProfesor/']);
	    },
      (err: any) => {
        console.log("Error: ", err);
        swal({
          title: 'Usuario o contraseña incorrectos',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar',
          confirmButtonClass: 'btn btn-success',
          buttonsStyling: false,
          reverseButtons: true
        })
      });
  }
}

        //switch(data.usuario.rolId.idRol){
       /** switch(data.usuario.status){
        case 'Activo':
            this.router.navigate(['/login']);
            break;
          case 2:
            this.router.navigate(['/siex/docente/inicio']);
            break;
          case 3:
            this.router.navigate(['/siex/alumno/inicio']);
            break;
        }**/
