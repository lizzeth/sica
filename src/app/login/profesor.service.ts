import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfesorService {

  private urlEndPoint: string = 'http://localhost:8080/';
    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

	constructor(private http: HttpClient){  }
 	
 
 	login(user){
		var headers = new HttpHeaders({'No-Auth':'True'});
		var data = this.http.post(this.urlEndPoint+'login', user , {headers: headers});
		console.log(this.http.post(this.urlEndPoint+'login', user , {headers: headers}));
		return data;
 	}
 
 	saveUsuario(user){
 		console.log(user);
		var headers = new HttpHeaders({'No-Auth':'True'});
		return this.http.post(this.urlEndPoint+'usuario', user , {headers: headers});

	 } 
	 
	allUsuarios(){
		return this.http.get(this.urlEndPoint +'usuario');
	}
}
