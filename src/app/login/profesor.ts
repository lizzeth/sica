export class Profesor {
    constructor(
        public id: string,
        public lastname: string,
        public nombre: string,
        public username: string,
        public password: string,
        public status: string,
        
    ) {
    }
}