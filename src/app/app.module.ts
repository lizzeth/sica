import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CardModule} from 'primeng/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import { AsignacionGrupoComponent } from './asignacion-grupo/asignacion-grupo.component';
import { AsignacionGrupoService } from './asignacion-grupo/asignacion-grupo.service';
import { LoginComponent } from './login/login.component';
import { CalificacionAlumnoComponent } from './calificacion-alumno/calificacion-alumno.component';
import { CalificacionAlumnoService } from './calificacion-alumno/calificacion-alumno.service';
import { AuthInterceptor } from './Auth/auth.interceptor';
import { AuthGuard } from './Auth/auth.guard';


const routes: Routes = [

  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},

  {path: 'asignacionGrupoProfesor', component: AsignacionGrupoComponent, canActivate:[AuthGuard]},
 

  {path: '', redirectTo: '/calificacionAlumno', pathMatch: 'full'},
  {path: 'calificacionAlumno/:id', component: CalificacionAlumnoComponent, canActivate:[AuthGuard]},
]


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    AsignacionGrupoComponent,
    LoginComponent,
    CalificacionAlumnoComponent,
   
  ],
  imports: [
    BrowserModule, 
    BrowserAnimationsModule,
    FormsModule,
    CardModule,
    HttpClientModule, 
    RouterModule.forRoot(routes)
  ],
  providers: [AuthGuard,
    {
      provide : HTTP_INTERCEPTORS,
      useClass : AuthInterceptor,
      multi : true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
